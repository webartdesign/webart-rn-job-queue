// import BackgroundQueue from './queue'
import { JobContext } from './Context'
import QueueRedux from './Redux/QueueRedux'

/**
 * job queue reducer
 */
const QueueReducer = require('./Redux/QueueRedux').reducer

export {
  JobContext,
  QueueRedux,
  QueueReducer
}

// export default BackgroundQueue
