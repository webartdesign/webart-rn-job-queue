import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import queueFactory from 'react-native-queue'
import QueueActions from './Redux/QueueRedux'
import {JobContext} from './Context'

class BackgroundTaskRunner extends Component {
  constructor (props) {
    super(props)

    /**
     * initial state
     */
    this.state = {
      queue: null
    }
  }

  componentDidMount = async () => {
    // initialize job
    this.initJobQueue()

    // create api service
    this.api = this.props.api
  }

  componentWillReceiveProps (newProps) {
    if (this.props.started && !newProps.started) {
      this.state.queue.stop()
    } else if (!this.props.started && newProps.started) {
      this.state.queue.start()
    }
  }

  /**
   * Init job queue,
   */
  initJobQueue = async () => {
    const queue = await queueFactory()

    /**
     * add post request worker
     */
    queue.addWorker('api-request', async (id, {apiName, payload, actionSuccess}) => {
      console.tron.log('api-request worker called')
      /**
       * TODO: trigger queue redux here?
       */
      await new Promise(async (resolve, reject) => {
        try {
          /**
           * call action to let know that this item is being queued!
           */
          this.props.processJob({
            id,
            apiName,
            payload
          })

          const axiosOptions = {
            onUploadProgress: (progressEvent) => {
              const percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total )

              /**
               * TODO: might be good to show a progress loader on the syncable component showing current job progress specially on upload type tasks
               */
            }
          }

          const response = await this.api[apiName](payload, axiosOptions)

          if (response.ok) {
            this.props.processJobSuccess({
              id,
              apiName,
              payload
            })

            if (actionSuccess) {
              const action = {
                ...actionSuccess,
                data: response.data
              }

              this.props.dispatchAction(action)
            }

            resolve()
          }
        } catch (err) {
          /**
           * something went wrong on the api call above
           */
          console.tron.log(err)
        }
      })
    }, {
      concurrency: 1,

      onStart: async (id) => {
        console.tron.log('Job "job-name-here" with id ' + id + ' has started processing.')
      },

      // onSuccess job callback handler is fired after a job successfully completes processing.
      onSuccess: async (id, payload) => {
        /**
         * TODO: trigger a redux related action here?
         * TODO: But how do we send the success data to redux action?? maybe as long as api response is ok then that's a total success??
         */
      },

      // onFailure job callback handler is fired after each time a job fails (onFailed also fires if job has reached max number of attempts).
      onFailure: async (id, {apiName, payload, actionSuccess, actionFailure}) => {
        /**
         * TODO: trigger a redux related action here?
         */

        console.tron.log(id)
      },

      // onFailed job callback handler is fired if job fails enough times to reach max number of attempts.
      onFailed: async (id, {apiName, payload, actionFailure}) => {
        /**
         * TODO: trigger a redux related action here?
         */

        this.props.processJobFailure({
          id,
          apiName,
          payload
        })
      },

      // onComplete job callback handler fires after job has completed processing successfully or failed entirely.
      onComplete: async (id, payload) => {
        console.tron.log('Job "job-name-here" with id ' + id + ' has completed processing.')
      }
    })

    this.setState({queue: queue})
  }

  /**
   *
   * @param payload - data payload on each job queue to process
   * @param actionSuccess - required, its a redux function that should be called when a job has successfully executed
   * @param actionFailure - required, its a redux function that should be called when a job completely fails after 5 attempts of retrying
   */
  createJob = (apiName, payload, actionSuccess, actionFailure) => {
    this.state.queue.createJob(
      'api-request',
      {
        apiName,
        payload,
        actionSuccess,
        actionFailure
      },
      {
        attempts: 1
      },
      this.props.started
    )
  }

  render () {
    if (!this.state.queue) return null
    // Background Geolocation service will always be active, but it will only
    // send employee's location if they are on a job or going to the customers site
    return (
      <JobContext.Provider value={{
        createJob: this.createJob
      }}>
        {this.props.children}
      </JobContext.Provider>

    )
  }
}

BackgroundTaskRunner.propTypes = {
  api: PropTypes.object,
}

const mapStateToProps = (state) => {
  return {
    started: state.jobQueue.started
  }
}

const mapDispatchToProps = (dispatch) => ({
  dispatchAction: (action) => dispatch(action),
  processJob: (data) => dispatch(QueueActions.processJob(data)),
  processJobSuccess: (data) => dispatch(QueueActions.processJobSuccess(data)),
  processJobFailure: (data) => dispatch(QueueActions.processJobFailure(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(BackgroundTaskRunner)
