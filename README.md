##  Task Queue Runner

A job queue runner

## How to Setup

```bash
$ yarn add https://github.com/rheilgm/react-native-queue.git
```

Then, because this package has a depedency on [Realm](https://github.com/realm/realm-js) you will need to link this native package by running:

```bash
$ react-native link realm
```

Wrap your root component.
Example below is from ignite andross boilerplate setup

```js

import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'
import ReduxNavigation from '../Navigation/ReduxNavigation'
import { connect } from 'react-redux'
import StartupActions from '../Redux/StartupRedux'
import ReduxPersist from '../Config/ReduxPersist'
import TaskQueue from 'webart-rn-job-queue'
import Secrets from 'react-native-config'
import API from '../Services/Api'

// Styles
import styles from './Styles/RootContainerStyles'

// create api so that job queue can use this to call your apisauce api's
const ServiceAPI = API.create(Secrets.API_URL)

class RootContainer extends Component {
  componentDidMount () {
    // if redux persist is not active fire startup action
    if (!ReduxPersist.active) {
      this.props.startup()
    }
  }

  render () {
    return (
      <View style={styles.applicationView}>
        <StatusBar barStyle='light-content' />
        
        <TaskQueue api={ServiceAPI}>
          <ReduxNavigation />
        </TaskQueue>
      </View>
    )
  }
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = (dispatch) => ({
  startup: () => dispatch(StartupActions.startup())
})

export default connect(null, mapDispatchToProps)(RootContainer)

```

You can then call createJob from any screens:

```js
    // First import context
    import { JobContext } from 'webart-rn-job-queue'

    export default class SampleComponent extends React.Component {
        // then you can use it inside of you component
        componentDidMount () {
            const { createJob } = this.props.screenProps
            
            // Call create job here
            // createJob ( apiSauceAPI, apiPayload )
            createJob('uploadJobPhoto', {photo: 'photo to upload here....'})
        }
    }
    
    // add job context to this sample component
    SampleComponent.contextType = JobContext
    
```

Lastly, add queue reducer to your app reducer

```js
    // import QueueReducer
    import { QueueReducer } from 'webart-rn-job-queue'
    
    /* ------------- Assemble The Reducers ------------- */
    export const reducers = combineReducers({
      ...All your reducers here,
      jobQueue: QueueReducer // add this 
    })
    
```

## API

#### createJob(API_NAME, PAYLOAD, REDUX_ACTION_SUCCESS, REDUX_ACTION_FAILURE)

this supports redux actions
```js
    import UploadActions from '../Redux/UploadRedux'
    
    // then we call ..
    
    createJob(
      'uploadJobPhoto',
      {photo: 'photo to upload here....'},
      UploadActions.uploadSuccess(),
      UploadActions.uploadFailure()
    )
```
after a job succeeded executing, it will call the generated success/failure action and then appends a `data` param to the redux action, be sure to setup your actions to use `data` if you needed the data returned by api ex:
```js
    const { Types, Creators } = createActions({
      uploadSuccess: ['data']
    })
```

or use it without actions
```js
    createJob(
      'uploadJobPhoto',
      {photo: 'photo to upload here....'}
    )
```