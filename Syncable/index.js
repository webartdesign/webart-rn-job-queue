import React from 'react'
import { connect } from 'react-redux'
import { View, ActivityIndicator } from 'react-native'
import { Button, Text, Icon } from 'native-base'
import styles from './styles'

class Syncable extends React.Component {
  render () {
    const {identifier, value} = this.props

    const isQueued = this.props.queue.find(q => q.payload[identifier] === value)

    return (
      <View style={styles.container}>
        {!!isQueued &&
          <View style={styles.loadingIcon}>
            {!isQueued.failed &&
              <ActivityIndicator size='small' color='#0000ff' />
            }

            {isQueued.failed &&
              <Button small iconRight style={styles.buttonFailed} danger>
                <Text>Sync failed</Text>
                <Icon name='warning' />
              </Button>
            }
          </View>
        }

        <View style={styles.wrapped}>
          {this.props.render(this)}
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    queue: state.jobQueue.data || []
  }
}

const mapDispatchToProps = (dispatch) => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(Syncable)
