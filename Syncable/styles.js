import { StyleSheet } from 'react-native'
import variables from 'Themes/variables/platform'
// import { Colors } from 'Themes/'

export default StyleSheet.create({
  container: {
    flexDirection: 'column'
  },

  wrapped: {},
  loadingIcon: {
    backgroundColor: variables.brandLight,
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginBottom: 7
  },

  buttonFailed: {
    alignSelf: 'flex-end'
  }
})
