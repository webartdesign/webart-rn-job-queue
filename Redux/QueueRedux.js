import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  workerToggle: ['value'],
  processJob: ['jobData'],
  processJobSuccess: ['response'],
  processJobFailure: ['response']
})

export const JobItemsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  started: false,
  data: []
})

/* ------------- Selectors ------------- */

export const JobItemsSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const process = (state, { jobData }) => {
  const dd = state.data || []
  const job = dd.find(a => a.payload.uniqID === jobData.payload.uniqID)

  if (!job) {
    return state.merge({data: [...dd, jobData]})
  } else {
    if (job.failed) {
      let updatedData = [...state.data].map(d => {
        if (d.payload.uniqID === jobData.payload.uniqID) {
          let pl = {...d}
          delete pl.failed

          console.tron.log(pl)

          return pl
        } else {
          return d
        }
      })

      return state.merge({data: updatedData})
    } else {
      return state
    }
  }
}

export const workerToggle = (state, { value }) => {
  return state.merge({started: value})
}

// successful api lookup
export const success = (state, {response}) => {
  const filtered = state.data.filter(d => d.payload.uniqID !== response.payload.uniqID)

  return state.merge({data: filtered})
}

// Something went wrong somewhere.
export const failure = (state, { response }) => {
  let updatedData = [...state.data].map(d => {
    if (d.payload.uniqID === response.payload.uniqID) {
      return {...d, failed: true}
    } else {
      return d
    }
  })

  return state.merge({data: updatedData})
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.WORKER_TOGGLE]: workerToggle,
  [Types.PROCESS_JOB]: process,
  [Types.PROCESS_JOB_SUCCESS]: success,
  [Types.PROCESS_JOB_FAILURE]: failure
})
